# Math >> ( Computing + Physics ) and I love Applied Math
import pygame
import random
from config import *

from pygame.locals import (
    K_UP,
    K_DOWN,
    K_LEFT,
    K_RIGHT,
    K_ESCAPE,
    KEYDOWN,
    QUIT,
)

'''# useful variables
FPS = 30
num_const_enemies = 4
min_mv_enemies = 1
max_mv_enemies = 2
min_speed = 4
max_speed = 8
max_death = 1
font_name = pygame.font.match_font('arial')'''

# initialise PyGame and create window
pygame.init()
screen = pygame.display.set_mode((800, 600))
pygame.display.set_caption("Melul Smokes but That ain't Wrong")
clock = pygame.time.Clock()
begin_image = pygame.image.load("begin.jpg")
begin_image = pygame.transform.scale(begin_image, (800, 600))
background_image = pygame.image.load("clouds.jpg")
background_image = pygame.transform.scale(background_image, (800, 600))
end_images = pygame.image.load("hell_end.jpg")
end_images = pygame.transform.scale(end_images, (800, 600))

# stripes
stripe_image = pygame.image.load("stripes.png").convert_alpha()
stripe_image = pygame.transform.scale(stripe_image, (800, 30)).convert_alpha()
end_image = pygame.image.load("ends.jpg").convert_alpha()
end_image = pygame.transform.scale(end_image, (800, 30)).convert_alpha()

# static enemy images
static_enemy = [pygame.image.load('crusade.png'),
                pygame.image.load('trump.png'),
                pygame.image.load('devil1.png'),
                pygame.image.load('black_knight.png')
                ]
dyn_enemy = [pygame.image.load('hitler.png'),
             pygame.image.load("gun.png")
             ]

'''# define colors
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255, 50)
RED_HIGHLIGHT = (240, 50, 50, 10)
PURPLE = (255, 0, 255)

# sprite for player
x = 400
y = 570
vel = 5'''


# define a module to draw text
def draw_text(surf, text, size, var_x, var_y, text_color):
    font = pygame.font.Font(font_name, size)
    text_surface = font.render(text, True, text_color)
    text_rect = text_surface.get_rect()
    text_rect.midtop = (var_x, var_y)
    surf.blit(text_surface, text_rect)


# Player Class
class Player(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load("jesus.png")
        self.image = pygame.transform.scale(self.image, (64, 64))
        self.rect = self.image.get_rect()
        self.radius = 10
        # pygame.draw.circle(self.image, RED, self.rect.center, self.radius)
        self.rect.center = (x, y)


# Constant Enemy Class
class ConstEnemies(pygame.sprite.Sprite):
    def __init__(self, stripe_num, partition):
        pygame.sprite.Sprite.__init__(self)
        random_image = random.randrange(0, 4, 1)
        self.image = static_enemy[random_image]
        self.image = pygame.transform.scale(self.image, (60, 56))
        self.rect = self.image.get_rect()
        self.radius = 20
        # pygame.draw.circle(self.image, RED, self.rect.center, self.radius)
        self.x_loc = random.randrange(20 + 190 * partition,
                                      190 + 190 * partition - 20, 1)
        self.y_loc = stripe_num * 120 + 90
        self.rect.center = (self.x_loc, self.y_loc)


# Moving Enemy Class
class MovEnemies(pygame.sprite.Sprite):
    def __init__(self, space_num):
        pygame.sprite.Sprite.__init__(self)
        random_image = random.randrange(0, 2, 1)
        self.image = dyn_enemy[random_image]
        self.image = pygame.transform.scale(self.image, (32, 32))
        self.rect = self.image.get_rect()
        self.radius = 10
        # pygame.draw.circle(self.image, RED, self.rect.center, self.radius)
        self.rect.x = -10
        self.rect.y = 30 + 120 * space_num
        self.speedx = random.randrange(min_speed, max_speed + 1, 1)

    def update(self):
        self.rect.x += self.speedx
        if self.rect.x > 800:
            self.rect.x = -10


# spawning static enemies
def spawn_static_enemies():
    for i in range(0, 4):
        for j in range(0, num_const_enemies):
            const_enemy = ConstEnemies(i, j)
            all_sprites.add(const_enemy)
            foe.add(const_enemy)


# spawning dynamic enemies
def spawn_dynamic_enemies():
    for i in range(0, 5):
        for j in range(0,
                       random.randrange(min_mv_enemies, max_mv_enemies + 1, 1)):
            mov_enemy = MovEnemies(i)
            all_sprites.add(mov_enemy)
            foe.add(mov_enemy)


# shape of the user
all_sprites = pygame.sprite.Group()
foe = pygame.sprite.Group()
player = Player()
spawn_static_enemies()
spawn_dynamic_enemies()
all_sprites.add(player)

# score initialised
score = 0
print_score = 0
player_1_score = 0
player_2_score = 0

# timer initialised
start_ticks = pygame.time.get_ticks()

# flag initialisation
flag = 0

# level initialisation
lvl = 0
player_1_lvl = 0
player_2_lvl = 0

# death initialisation
player_1_deaths = 0
player_2_deaths = 0

# starting the game
start_flag = 1

run = True
while run:
    clock.tick(FPS)

    # starting
    '''if start_flag == 1:
        start_flag = 0
        screen.blit(begin_image, (0, 0))
        pygame.display.update()
        pygame.time.delay(6000)
        draw_text(screen, "MELUL SAVES JESUS", 50, 400, 300, BLACK)
        draw_text(screen, " a game by Alapan Chaudhuri", 20, 730, 580, RED)
        pygame.display.update()
        pygame.time.delay(6000)'''

    # reduce timer
    seconds = 60 - (pygame.time.get_ticks() - start_ticks) // 1000

    # check for closing the window
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False
    # update
    keys = pygame.key.get_pressed()
    if keys[K_LEFT]:
        x -= vel
    if keys[K_RIGHT]:
        x += vel
    if keys[K_UP]:
        y -= vel
    if keys[K_DOWN]:
        y += vel

    if player.rect.bottom > 600:
        y -= 3
    if player.rect.top < 0:
        y += 3
    if player.rect.left < 0:
        x += 3
    if player.rect.right > 800:
        x -= 3

    player.rect.center = (x, y)
    all_sprites.update()

    # check if a enemy hit the player or time got elapsed completely
    hits = pygame.sprite.spritecollide(player, foe, False,
                                       pygame.sprite.collide_circle)
    if (hits and 50 < y < 550) or (seconds == 0):
        if flag == 0:
            player_1_score += score
            player_1_deaths += 1
            draw_text(screen, "PLAYER ONE IS DEAD", 40, 400, 200, BLACK)
            draw_text(screen, "PLAYER TWO BE READY", 40, 400, 300, WHITE)
            pygame.display.update()
            pygame.time.delay(1000)
            print_score = player_2_score
            score = 0
            player_1_lvl = lvl
            lvl = player_2_lvl
        else:
            player_2_score += score
            player_2_deaths += 1
            draw_text(screen, "PLAYER TWO IS DEAD", 40, 400, 200, BLACK)
            draw_text(screen, "PLAYER ONE BE READY", 40, 400, 300, WHITE)
            pygame.display.update()
            pygame.time.delay(1000)
            print_score = player_1_score
            score = 0
            player_2_lvl = lvl
            lvl = player_1_lvl
        x = 400
        y = 570
        vel = 5 + lvl // 3
        min_mv_enemies = 1 + lvl
        max_mv_enemies = 2 + lvl * 2
        min_speed = 4 + lvl
        max_speed = 8 + lvl * 2
        spawn_dynamic_enemies()
        start_ticks = pygame.time.get_ticks()
        flag ^= 1

    # check if time elapsed completely
    '''if seconds == 0:
        x = 400
        y = 570
        vel = 5
        score = 0
        start_ticks = pygame.time.get_ticks()'''

    # death counter checker to end the game
    if player_1_deaths > max_death and player_2_deaths > max_death:
        player_1_score -= 20 * player_1_deaths
        player_2_score -= 20 * player_2_deaths
        screen.blit(end_images, (0, 0))
        pygame.display.update()
        if player_1_score > player_2_score:
            draw_text(screen, "JESUS LOVES PLAYER ONE WHO WON", 40, 400, 100,
                      WHITE)
            draw_text(screen, "AND PLAYER TWO WELCOME TO ISS", 40, 400, 500,
                      WHITE)
        else:
            draw_text(screen, "JESUS LOVES PLAYER TWO WHO WON", 40, 400, 100,
                      WHITE)
            draw_text(screen, "AND PLAYER ONE WELCOME TO ISS", 40, 400, 500,
                      WHITE)
        pygame.display.update()
        pygame.time.delay(6000)
        run = False

    # increase score
    if y < 90 and score < 40:
        score = 40
    elif y < 210 and score < 30:
        score = 30
    elif y < 330 and score < 20:
        score = 20
    elif y < 450 and score < 10:
        score = 10

    # check if the player completes his turn
    if 350 < x < 450 and 0 < y < 20:
        if flag == 0:
            player_1_score += (score + seconds)
            draw_text(screen, "PLAYER ONE CONGRATS", 40, 400, 200, BLACK)
            draw_text(screen, "PLAYER TWO BE READY", 40, 400, 300, WHITE)
            pygame.display.update()
            pygame.time.delay(1000)
            print_score = player_2_score
            score = 0
            player_1_lvl = lvl + 1
            lvl = player_2_lvl
        else:
            player_2_score += (score + seconds)
            draw_text(screen, "PLAYER TWO CONGRATS", 40, 400, 200, BLACK)
            draw_text(screen, "PLAYER ONE BE READY", 40, 400, 300, WHITE)
            pygame.display.update()
            pygame.time.delay(1000)
            print_score = player_1_score
            score = 0
            player_2_lvl = lvl + 1
            lvl = player_1_lvl
        x = 400
        y = 570
        vel = 5 + lvl // 3
        min_mv_enemies = 1 + lvl
        max_mv_enemies = 2 + lvl * 2
        min_speed = 4 + lvl
        max_speed = 8 + lvl * 2
        spawn_dynamic_enemies()
        start_ticks = pygame.time.get_ticks()
        flag ^= 1

    # draw and render
    screen.blit(background_image, (0, 0))
    screen.blit(end_image, (0, 570))
    screen.blit(stripe_image, (0, 450))
    screen.blit(stripe_image, (0, 330))
    screen.blit(stripe_image, (0, 210))
    screen.blit(stripe_image, (0, 90))
    screen.blit(end_image, (0, 0))
    all_sprites.draw(screen)

    # draw text for score
    txt_score = "SCORE = " + str(print_score)
    draw_text(screen, txt_score, 18, 50, 10, WHITE)
    # draw text for time left
    txt_sec = "TIME LEFT = " + str(seconds) + " sec"
    draw_text(screen, txt_sec, 18, 730, 10, BLACK)

    # draw start and end of the game
    draw_text(screen, "END", 18, 390, 10, WHITE)
    draw_text(screen, "START", 18, 390, 580, BLACK)

    # level and player
    draw_text(screen, "LEVEL = " + str(lvl), 18, 50, 580, WHITE)
    draw_text(screen, "PLAYER = " + str(flag + 1), 18, 730, 580, BLACK)

    # updating the game
    pygame.display.update()

pygame.quit()
