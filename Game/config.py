import pygame

# useful variables
FPS = 30
num_const_enemies = 4
min_mv_enemies = 1
max_mv_enemies = 2
min_speed = 4
max_speed = 8
max_death = 1
font_name = pygame.font.match_font('arial')

# define colors
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255, 50)
RED_HIGHLIGHT = (240, 50, 50, 10)
PURPLE = (255, 0, 255)

# sprite for player
x = 400
y = 570
vel = 5